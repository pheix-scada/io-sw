#!/usr/bin/env perl

use feature 'say';
use strict;
use warnings;

use JSON::Validator;
use File::Basename;
use File::Slurp;
use JSON qw(decode_json);
use Encode qw(encode_utf8);

use constant JSONPATH => 'perl/schemas/v2';
use constant JSONTEST => 'perl/schemas/v2/test';

my @cases = glob sprintf( "%s/*", JSONTEST );

my $ok = 1;
my $jv = JSON::Validator->new;

foreach my $case (@cases) {
    my $json;
    my $file = basename($case);

    ( my $index = $file ) =~
      s/([0-9]{2})\-(read|write)\-(tags|debug|status|soe)\-[0-9]\.json$/$1/;
    ( my $request = $file ) =~
      s/([0-9]{2})\-(read|write)\-(tags|debug|status|soe)\-[0-9]\.json$/$2/;
    ( my $group = $file ) =~
      s/([0-9]{2})\-(read|write)\-(tags|debug|status|soe)\-[0-9]\.json$/$3/;

    $jv->schema(
        sprintf(
            "file://%s/json/%s/%s-%s.json",
            JSONPATH, $request, $index, $group
        )
    );

    eval { $json = decode_json( encode_utf8( read_file($case) ) ); 1; } or do {
        my $error = $@ || 'Unknown failure';

        say sprintf( "validation for %s is failed, json parsing error: %s",
            $case, $error )
          and $ok = 0;

        last;
    };

    my @errors = $jv->validate($json);

    if (@errors) {
        say sprintf( "validation for %s is failed: %s",
            $case, Data::Dumper::Dumper( \@errors ) )
          and $ok = 0;

        last;
    }
}

if ($ok) {
    say sprintf( "***INF: validation is successful for %d requests!",
        scalar(@cases) );
    exit 0;
}
else {
    say "***ERR: validation is finished with failures";
    exit 1;
}

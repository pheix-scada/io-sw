#/bin/bash

APACHE_RESP=404

/usr/bin/sudo -u postgres /usr/local/pgsql/bin/pg_ctl start --pgdata=/usr/local/pgsql/data/ > /dev/null 2>&1 &

sleep 5

while [ $APACHE_RESP -ne 200 ]; do
	/usr/local/apache2/bin/apachectl restart > /dev/null 2>&1

	APACHE_RESP=$(curl --write-out '%{http_code}' --silent --output /dev/null http://127.0.0.1:8888/request-r.html);
	sleep 1;
done;

/usr/bin/sudo -u postgres /usr/local/pgsql/bin/psql -c "ALTER USER postgres WITH ENCRYPTED PASSWORD 'ospasswd';" > /dev/null 2>&1

echo "IO server is started"

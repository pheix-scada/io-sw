use strict;
use warnings;

use Test::More;

plan tests => 1;

is(
    request_ioserver('http://127.0.0.1:8888/request-r.html', q{}),
    -1,
    'Invalid request to IO server'
);

done_testing();

sub request_ioserver {
    my ($url,$json) = @_;
    return -1;
}

# New features

1. Send/receive string messages for debugging (beremiz debugging);
2. Send/receive sequences of events (both on MLCP and database functions);
3. Send/receive controller statuses (needed by reserving model);
4. Send/receive tags and commands (previously implemented);

## String messages for debugging

* Read request/response: [/perl/schemas/v2/json/read/01-debug.json](/perl/schemas/v2/json/read/01-debug.json)
* Write request/response: [/perl/schemas/v2/json/write/01-debug.json](/perl/schemas/v2/json/write/01-debug.json)

## Sequences of events

* Read request/response: [/perl/schemas/v2/json/read/03-soe.json](/perl/schemas/v2/json/read/03-soe.json)
* Write request/response: [/perl/schemas/v2/json/write/03-soe.json](/perl/schemas/v2/json/write/03-soe.json)

## Controller statuses

* Read request/response: [/perl/schemas/v2/json/read/02-status.json](/perl/schemas/v2/json/read/02-status.json)
* Write request/response: [/perl/schemas/v2/json/write/02-status.json](/perl/schemas/v2/json/write/02-status.json)

## Tags and commands

* Read request/response: [/perl/schemas/v2/json/read/00-tags.json](/perl/schemas/v2/json/read/00-tags.json)
* Write request/response: [/perl/schemas/v2/json/write/00-tags.json](/perl/schemas/v2/json/write/00-tags.json)

## License

**IO-sw** is FOSS, so you can redistribute it and/or modify it under the terms of the [The Artistic License 2.0](https://opensource.org/licenses/Artistic-2.0).

## Author

Please contact me via [LinkedIn](https://www.linkedin.com/in/knarkhov/) or [Twitter](https://twitter.com/CondemnedCell). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).

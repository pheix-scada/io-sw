#!/usr/bin/env perl

use strict;
use warnings;

use lib './IO';

use CGI::Carp qw(fatalsToBrowser);
use CGI;
use POSIX;
use JSON qw(encode_json decode_json);
use JSON::Validator;
use Encode qw(encode decode encode_utf8 decode_utf8);
use Data::Dumper;
use IODB;
use File::Slurp;

use constant DODUMP   => 1;
use constant JSONPATH => 'file://schemas/v1/json';

my $co = CGI->new;
my $jv = JSON::Validator->new;
my %env_params;

if ( defined $co->url_param() ) {
    %env_params = map { $_ => $co->url_param($_) } $co->url_param();
}

my %r_response = (
    status     => -1,
    message    => 'invalid request !!!!',
    r_response => { tags => [] }
);

if (   ( exists( $env_params{action} ) )
    && ( $env_params{action} eq 'request-r' ) )
{
    my $cli_json;
    my @errors = ('no post data');

    $jv->schema( sprintf( "%s/read_tag_comm_request.json", JSONPATH ) );

    if ( defined $co->param("POSTDATA") ) {
        my $post_req = $co->param("POSTDATA");

        eval { $cli_json = decode_json( encode_utf8($post_req) ) };

        if ($@) {
            my $fn = sprintf( "./logs/requests/%d.json", time );
            write_file( sprintf( $fn, $post_req ) ) if (DODUMP);

            push @errors, "request parse failure, dump saved to " . $fn;
        }
        else {
            @errors = $jv->validate($cli_json);
        }
    }

    if ( !@errors ) {
        logdumper( $co->param("POSTDATA") );
        my @r_tags;
        my $message;
        my $cntrl_id = $cli_json->{'controller_id'} || 0;
        if ( defined $cli_json->{'r_request'}->{'tags'} ) {
            foreach ( @{ $cli_json->{'r_request'}->{'tags'} } ) {
                push(
                    @r_tags,
                    {
                        tag   => $_,
                        value => 0,
                        mode  => 'r'
                    }
                );
            }

            logdumper( \@r_tags );

            if ( @r_tags && $cntrl_id > 0 ) {
                my @tuples;
                my @names = map { $_->{tag} } @r_tags;
                my $valref;

                eval {
                    $valref = IODB::iodb_select( \@names, $#names + 1, $cntrl_id )
                      if not defined $env_params{test};
                };

                if ($@) {
                    $message = $@;
                }
                else {
                    if ( defined $valref && @$valref ) {
                        logdumper($valref);
                        my $index = 0;

                        for my $r_tag (@r_tags) {
                            $r_tag->{value} = @$valref[$index];
                            push( @tuples, $r_tag->{tag} . ":" . $r_tag->{value} );
                            $index++;
                        }

                        $message = "tags are successfully fetched from database";
                        logdumper($message);
                    }
                    else {
                        for my $r_tag (@r_tags) {
                            $r_tag->{value} = sprintf( "%.3f", rand(200) ) * 1;
                            push( @tuples, $r_tag->{tag} . ":" . $r_tag->{value} );
                        }
                        $message = "tags are randomly generated, possible no records in database";
                        logdumper($message);
                    }

                    open my $fh, ">>", "logs/database.log";
                    print $fh $cntrl_id . q{:} . join( ", ", @tuples ) . "\n";
                    close $fh;
                }
            }
        }
        else {
            open my $fh, ">>", "logs/command.log";

            print $fh localtime . "\t"
              . $ENV{REMOTE_ADDR} . "\t"
              . $cli_json->{'r_request'}->{'command'}->{'comm_id'} . "\t"
              . $cli_json->{'r_request'}->{'command'}->{'data'} . "\n";

            close $fh;

            $message = "command is successfully handled and logged";
        }

        %r_response = (
            status     => 0,
            message    => substr($message, 0, 255),
            r_response => { tags => [@r_tags] }
        );

        logdumper( \%r_response );
        $jv->schema( sprintf( "%s/read_tag_response.json", JSONPATH ) );

        my @e = $jv->validate( \%r_response );

        if ( !@e ) {
            logdumper("Response is successfully validated");
            my $response = $co->header( -type => 'application/json' ) . encode_json( \%r_response );

            logdumper($response);
            print $response;
        }
        else {
            logdumper("response validation failure");
            print( $co->header( -type => 'application/json' )
                  . encode_json(
                    {
                        status     => -1,
                        message    => join( q{,}, @e ),
                        r_response => { tags => [] }
                    }
                  )
            );
        }
    }
    else {
        print( $co->header( -type => 'application/json' )
              . encode_json(
                {
                    status     => -1,
                    message    => join( q{,}, @errors ),
                    r_response => { tags => [] }
                }
              )
        );
    }

    exit;
}

if (   ( exists( $env_params{action} ) )
    && ( $env_params{action} eq 'request-w' ) )
{
    my $cli_json;
    my @errors = ('no post data');
    $jv->schema( sprintf( "%s/write_tag_request.json", JSONPATH ) );

    if ( defined $co->param("POSTDATA") ) {
        my $post_req = $co->param("POSTDATA");

        eval { $cli_json = decode_json( encode_utf8($post_req) ) };

        if ($@) {
            my $fn = sprintf( "./logs/requests/%d.json", time );
            write_file( $fn, $post_req ) if (DODUMP);

            push @errors, "request parse failure, dump saved to" . $fn;
        }
        else {
            @errors = $jv->validate($cli_json);
        }
    }
    if ( !@errors ) {
        my $message;
        my @names;
        my @values;
        my $dbsavestatus = 'false';
        my $cntrl_id     = $cli_json->{'controller_id'} || 0;

        foreach ( @{ $cli_json->{'w_request'}->{'tags'} } ) {
            push( @names,  $_->{tag} )   if $_->{tag} ne q{};
            push( @values, $_->{value} ) if defined $_->{value};
        }

        if ( @values && @names && ( $#values == $#names ) && $cntrl_id > 0 ) {
            my $retcode;

            my $db_insert_status = eval {
                $retcode = IODB::iodb_insert( \@names, \@values, $#names + 1, $cntrl_id );

                1;
            } or do {
                my $error_msg = $@;

                $message = sprintf( "iodb exception %s", quotemeta($error_msg) );
            };

            if ($retcode) {
                $dbsavestatus = 'true';
                $message      = 'tags push ok';
            }
            else {
                $message = 'tags push failure';
            }
        }

        open my $fh, '>>', 'logs/post.log';
        print $fh localtime . "\t"
          . $ENV{REMOTE_ADDR} . "\t"
          . ( $#{ $cli_json->{'w_request'}->{'tags'} } + 1 ) . "\t"
          . $cntrl_id . "\t"
          . $dbsavestatus . "\n";
        close $fh;

        my %w_response = (
            status  => ( ( $dbsavestatus eq "true" || ( $env_params{test} && $env_params{test} == 1 ) ) ? 0 : -1 ),
            message => substr(sprintf( "%s, database status %s", $message, $dbsavestatus ), 0, 255),
        );

        $jv->schema( sprintf( "%s/write_tag_response.json", JSONPATH ) );
        my @e = $jv->validate( \%w_response );

        if ( !@e ) {
            print( $co->header( -type => 'application/json' ) . encode_json( \%w_response ) );
        }
        else {
            print( $co->header( -type => 'application/json' )
                  . encode_json(
                    {
                        status  => -1,
                        message => substr(join( q{,}, @e ), 0, 255),
                    }
                  )
            );
        }
    }
    else {
        print( $co->header( -type => 'application/json' )
              . encode_json(
                {
                    status     => -1,
                    message    => substr(join( q{,}, @errors ), 0, 255),
                    w_response => { tags => [] }
                }
              )
        );
    }

    exit;
}

print( $co->header( -type => 'application/json' ) . encode_json( \%r_response ) );

sub logdumper {
    return unless (DODUMP);

    my $data  = shift;
    my $date  = localtime;
    my $delim = '-------------------------------------------------------------';
    my $hdr   = sprintf( "***** %s from %s *****", $date, $ENV{REMOTE_ADDR} );
    my $body  = Dumper($data);

    open my $fh, ">>", "logs/pingpong.log";
    print $fh sprintf( "%s\n%s\n%s%s\n", $delim, $hdr, $body, $delim );
    close $fh;
}
